#include "stdafx.h"
#include <string>
#include "vehicle_base.h"
#include "station_base.h"
#include "strings_func.h"
#include "town_map.h"
#include "town.h"
#include "townname_func.h"
#include <iostream>
#include <fstream>
#include "LogEvents.h"
#include <ctime>

bool LogVehicleEvent(Vehicle *v)
{
	/*
	OwnerByte owner = v->owner;													///< Which company owns the vehicle?
	EngineID engine_type = v->engine_type;										///< The type of engine used for this vehicle.
	int CargoDaysInTransit = &v->cargo.DaysInTransit;							///< Number of days cargo has been in transit
	CargoID cargo_type = this->cargo_type;										///< type of cargo this vehicle is carrying
	byte cargo_subtype = this->cargo_subtype;									///< Used for livery refits (NewGRF variations)
	uint16 cargo_cap = this->cargo_cap;											///< total capacity
	uint16 refit_cap = this->refit_cap;											///< Capacity left over from before last refit.
	VehicleCargoList cargo = this->cargo;										///< The cargo this vehicle is carrying
	int8 trip_occupancy = this->trip_occupancy;									///< NOSAVE: Occupancy of vehicle of the current trip (updated after leaving a station).
	byte day_counter = this->day_counter;										///< Increased by one for each day
	byte vehstatus = this->vehstatus;											///< Status
	*/

	Rect coord = v->coord;														///< NOSAVE: Graphical bounding box of the vehicle, i.e. what to redraw on moves.	
	Money value = v->value;														///< Value of the vehicle
	Money profit_this_year = v->profit_this_year;								///< Profit this year << 8, low 8 bits are fract
	Money profit_last_year = v->profit_last_year;								///< Profit last year << 8, low 8 bits are fract
	Year build_year = v->build_year;											///< Year the vehicle has been built.
	Date max_age = v->max_age;													///< Maximum age
	Date age = v->age;															///< Age in days
	UnitID unitnumber = v->unitnumber;											///< unit number, for display purposes only
	uint16 cur_speed = v->cur_speed;											///< current speed
	Date date_of_last_service = v->date_of_last_service;						///< Last date the vehicle had a service at a depot.
	byte breakdowns_since_last_service = v->breakdowns_since_last_service;		///< Counter for the amount of breakdowns.
	Order current_order = v->current_order;										///< The current order (+ status, like: loading)
	StationID last_station_visited = v->last_station_visited;					///< The last station we stopped at.
	StationID last_loading_station = v->last_loading_station;					///< Last station the vehicle has stopped at and could possibly leave from with any cargo loaded.															  

	if (unitnumber != 0)
	{
		string name;
		string type;
		time_t now = time(0);

		switch (v->type) {
		case VEH_TRAIN:    type = "Train"; break;
		case VEH_ROAD:     type = "Road Vehicle"; break;
		case VEH_SHIP:     type = "Ship"; break;
		case VEH_AIRCRAFT: type = "Aircraft"; break;
		default:           type = "Unknown Vehicle";
		}

		if (v->name != NULL)
		{
			name = v->name;
		}
		else
		{
			name = type + " " + to_string(unitnumber);
		}

		string lastStationVisited = GetTheStationName(last_station_visited);
		string lastLoadingStation = GetTheStationName(last_loading_station);

		//Build message
		string message = "{";
		message = message + "\"Name\":\"" + name + "\"";
		message = message + ",";
		message = message + "\"TimeStamp\":\"" + to_string(now) + "\"";
		message = message + ",";
		message = message + "\"Type\":\"" + type + "\"";
		message = message + ",";
		message = message + "\"CurrentSpeed\":" + to_string(cur_speed);
		message = message + ",";
		message = message + "\"LastStationVisited\":\"" + lastStationVisited + "\"";
		message = message + ",";
		message = message + "\"LastLoadingStation\":\"" + lastLoadingStation + "\"";
		message = message + ",";
		message = message + "\"ProfitLastYear\":" + to_string(profit_last_year);
		message = message + ",";
		message = message + "\"ProfitThisYear\":" + to_string(profit_this_year);
		message = message + ",";
		message = message + "\"Value\":" + to_string(value);
		message = message + ",";
		message = message + "\"BuildYear\":" + to_string(build_year);
		message = message + ",";
		message = message + "\"Age\":" + to_string(age);
		message = message + ",";
		message = message + "\"MaxAge\":" + to_string(max_age);
		message = message + ",";
		message = message + "\"DateOfLastService\":" + to_string(date_of_last_service);
		message = message + ",";
		message = message + "\"BreakdownsSinceLastService\":" + to_string(breakdowns_since_last_service);
		message = message + "}";

		//Write out message to temporary file
		ofstream myfile;
		string tempFolder = getenv("TEMP");
		myfile.open(tempFolder + "\\" + GetRandomString(15, "abcdefghijklmnaoqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890") + ".txt");
		myfile << message + "\n";
		myfile.close();

		return true;
	}
}

//Get Station Name from Station ID
string GetTheStationName(StationID s)
{
	string lastVisitedStation = "";
	try
	{
		BaseStation *station = BaseStation::Get(s);

		Town *town = station->town;

		char buf1[(MAX_LENGTH_TOWN_NAME_CHARS + 1) * MAX_CHAR_LENGTH];
		char buf2[(MAX_LENGTH_TOWN_NAME_CHARS + 1) * MAX_CHAR_LENGTH];

		const char *buf = town->name;
		if (buf == NULL) {
			GetTownName(buf2, town, lastof(buf2));
			buf = buf2;
		}

		StringID stationStringID = station->string_id;
		string stationNamePart = GetStringPtr(stationStringID);
		stationNamePart = stationNamePart.substr(stationNamePart.find(" ") + 1);

		string townNameString(buf);

		lastVisitedStation = townNameString + " " + stationNamePart;
	}
	catch (...)
	{

	}
	return lastVisitedStation;
}

//Generate Random String
string GetRandomString(uint l = 15, std::string charIndex = "abcdefghijklmnaoqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")
{
	// l and charIndex can be customized, but I've also initialized them.

	uint length = rand() % l + 1;
	// length of the string is a random value that can be up to 'l' characters.

	uint ri[15];
	/* array of random values that will be used to iterate through random
	indexes of 'charIndex' */

	for (uint i = 0; i < length; ++i)
		ri[i] = rand() % charIndex.length();
	// assigns a random number to each index of "ri"

	std::string rs = "";
	// random string that will be returned by this function

	for (uint i = 0; i < length; ++i)
		rs += charIndex[ri[i]];
	// appends a random amount of random characters to "rs"

	if (rs.empty()) GetRandomString(l, charIndex);
	// if the outcome is empty, then redo the generation (this doesn't work, help?)
	else return rs;
}