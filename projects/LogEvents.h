#pragma once

#include "stdafx.h"
#include <string>
#include "vehicle_base.h"
#include "station_base.h"
#include "strings_func.h"
#include "town_map.h"
#include "town.h"
#include "townname_func.h"
#include <iostream>
#include <fstream>

using namespace std;

bool LogVehicleEvent(Vehicle *v);
string GetTheStationName(StationID s);
string GetRandomString(uint l, std::string charIndex);